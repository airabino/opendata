# OpenData

This repository holds processed data for the SAE paper:

Synchronous and Open, Real World, Vehicle, ADAS, and Infrastructure Data Streams for Automotive Machine Learning Algorithms Research

Unfortunately; un-processed data cannot be uploaded at this time due to confidentiality agreements with various entities. I will continue 
to work on a solution to this.

The processed data is of the form:

DataStruct:

- All data is sampled at 10 Hz and is synchronized
- The 20 instances of the structure represent 20 laps along the same path
- The 20 laps are sampled from three separate drive cycles which were conducted on 3 consecutive days (10/21/2019-10/23/2019)
	- Laps 1-8 belong to the 10/21 drive
	- Laps 9-13 belong to the 10/22 drive
	- Laps 14-20 belong to the 10/23 drive
- The drive cycles from 10/21 and 10/22 were performed by the same driver while the 10/23 cycles were performed by a different driver.

DataStruct Fields:

- Time is reported in seconds from the stert of recording
- Latitude and Longitude are reported in degrees
- Distance in reported in meters and is the distance covered by the vehicle
- Acceleraore and Brake pedal positions are in percentage of available travel
- Transmission gears are reported simply as the gear index - ratios could not be provided as these are confidential
- Engine Speed is reported in RPM
- Engine torques are reported in Newton-meters. The min, max, and current torques are estimated values produced by the probe vehicle
- Turn signal values are trinary; 0-none, 1-left, 2-right
- Vehicle speed is reported in KPH
- Accelerations are reported in m/s^2
- Yaw rate is reported in degrees/second
- Altitude is reported in meters above Mean Sea Level
- Steer angle is in degrees as seen at the steering wheel
- SPaT consists of:
	- Distance to traffic light N along path [m]
	- Booleans for the potential phases of the traffic lights in the path direction (red, yellow, or green)
- SS consists of:
	- segment 5 minute rolling average speed [kph]
	- distnace remaining along segment [m]
- LV consists of:
	- dx: longitudinal distnace to lead vehicle from Ego vehicle [m]
	- dy: lateral distnace to lead vehicle from Ego vehicle [m]
- HS consists of:
	- Average speed of the vehicle at the vehicle's approximate current location in the opther laps
	- Standard deviation of speed of the vehicle at the vehicle's approximate current location in the other 19 laps
- Explicit boolean for when the vehicle is "stopped" - allows for the vehicle to creep forward at a traffic light and still be 
  considered "stopped"